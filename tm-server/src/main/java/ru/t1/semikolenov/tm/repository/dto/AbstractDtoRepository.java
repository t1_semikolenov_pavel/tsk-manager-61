package ru.t1.semikolenov.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.semikolenov.tm.dto.model.AbstractModelDTO;

@NoRepositoryBean
public interface AbstractDtoRepository<M extends AbstractModelDTO> extends JpaRepository<M, String> {

}
